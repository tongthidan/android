package com.example.baiorder;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;

public class UpdateActivity extends AppCompatActivity {
    TextView ratingvalue;
    EditText editID, editName, editPrice;
    Button btnUpdate, btnBack, btnDelete,btnDatePicker;
    RatingBar ratingBar;
    SQLiteOrderHelper sqLite;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update);
        getView();
        sqLite=new SQLiteOrderHelper(this);

        Intent intent = getIntent();
        if(intent != null) {
            Order order = (Order) intent.getSerializableExtra("order");
            String name = order.getNameItem();
            Integer id = order.getId();
            Float price = order.getPrice();
            String dateOrder = order.getDateOrder();
            String rating = order.getRating();
            editID.setText(id + "");
            editName.setText(name);
            editPrice.setText(price + "");
            ratingvalue.setText(rating);
        }

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    int id=Integer.parseInt(editID.getText().toString());
                    String n=editName.getText().toString();
                    String dateOrder = btnDatePicker.getText().toString();
                    Float price = Float.parseFloat(editPrice.getText().toString());
                    String rating = ratingvalue.getText().toString();
                    Order s=new Order(id,n,dateOrder,price,rating);
                    sqLite.update(s);

                    finish();
                }catch(NumberFormatException e){
                    System.out.println(e);
                }
            }
        });
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    int id=Integer.parseInt(editID.getText().toString());
                    sqLite.delete(id);
                    finish();
                }catch(NumberFormatException e){
                    System.out.println(e);
                }
            }
        });
    }
    void getView(){

        editID = findViewById(R.id.add_id);
        editName =  findViewById(R.id.add_name);
        editPrice =  findViewById(R.id.add_price);
        ratingBar =  findViewById(R.id.update_rating);
        btnDatePicker = findViewById(R.id.add_date);
        btnUpdate =  findViewById(R.id.btn_update);
        btnBack =  findViewById(R.id.btn_back);
        ratingvalue = findViewById(R.id.rating);
        btnDelete = findViewById(R.id.btn_delete);
    }
}