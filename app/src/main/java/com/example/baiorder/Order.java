package com.example.baiorder;

import java.io.Serializable;

public class Order implements Serializable {
    int id;
    String nameItem;
    String dateOrder;
    float price;
    String rating;

    public Order() {
    }

    public Order(String nameItem, String dateOrder, float price, String rating) {
        this.nameItem = nameItem;
        this.dateOrder = dateOrder;
        this.price = price;
        this.rating = rating;
    }

    public Order(int id, String nameItem, String dateOrder, float price, String rating) {
        this.id = id;
        this.nameItem = nameItem;
        this.dateOrder = dateOrder;
        this.price = price;
        this.rating = rating;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNameItem() {
        return nameItem;
    }

    public void setNameItem(String nameItem) {
        this.nameItem = nameItem;
    }

    public String getDateOrder() {
        return dateOrder;
    }

    public void setDateOrder(String dateOrder) {
        this.dateOrder = dateOrder;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }
}
