package com.example.baiorder;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.Calendar;

public class AddActivity extends AppCompatActivity {
    TextView ratingvalue;
    EditText editID, editName, editPrice;
    Button btnAdd, btnBack, btnDatePicker;
    RatingBar ratingBar;
    SQLiteOrderHelper sqLite;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);
       sqLite = new SQLiteOrderHelper(getApplicationContext());
        getView();
        getIntent();
        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                ratingvalue.setText(""+rating);
            }
        });
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Order order = new Order();
                String nameItem = editName.getText().toString();
                Float price = Float.parseFloat(editPrice.getText().toString());
                String dateOrder = btnDatePicker.getText().toString();
                String rating = ratingvalue.getText().toString();
                order.setNameItem(nameItem);
                order.setDateOrder(dateOrder);
                order.setPrice(price);
                order.setRating(rating);
                sqLite.addOrder(order);
                System.out.println("da luu");
                finish();
                startActivity(new Intent(AddActivity.this, MainActivity.class));
            }
        });
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnDatePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("AppLog", "Click");
                datePicker();
            }
        });
    }



    String date_time ="";
    int mYear, mMonth, mDay;
    void datePicker(){
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                        date_time = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;

                        btnDatePicker.setText(date_time);

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }
    void getView(){

        editID = findViewById(R.id.add_id);
        editName =  findViewById(R.id.add_name);
        editPrice =  findViewById(R.id.add_price);
        ratingBar =  findViewById(R.id.add_rating);
        btnDatePicker = findViewById(R.id.add_date);
        btnAdd =  findViewById(R.id.btn_add);
        btnBack =  findViewById(R.id.btn_back);
        ratingvalue = findViewById(R.id.rating);
    }
}