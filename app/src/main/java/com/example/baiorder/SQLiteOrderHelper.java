package com.example.baiorder;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

import java.util.ArrayList;
import java.util.List;

public class SQLiteOrderHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME="Order.db";
    private static final int DATABSE_VERSION=1;

    public SQLiteOrderHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABSE_VERSION);
    }



    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql="CREATE TABLE OrderDB (" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "nameItem TEXT," +
                "dateOrder TEXT," +
                "price REAL,"+" rating TEXT)";
        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
    }
    //addOrder
    void addOrder(Order order){
        String sql="INSERT INTO OrderDB (nameItem,dateOrder,price, rating) VALUES(?,?,?,?)";
        String[] args = {order.getNameItem(), order.getDateOrder(),
                Float.toString(order.getPrice()), order.getRating()};
        SQLiteDatabase statement = getWritableDatabase();
        statement.execSQL(sql,args);
    }
    //getAll
    public List<Order> getAll(){
        List<Order> list=new ArrayList<>();
        SQLiteDatabase statement=getReadableDatabase();
        Cursor rs=statement.query("OrderDB",null,
                null,null,null,
                null,null);
        while((rs!=null && rs.moveToNext())){
            Integer id = rs.getInt(0);
            String name=rs.getString(1);
            String dateOrder =rs.getString(2);
            float price=rs.getFloat(3);
            String rating = rs.getString(4);
            list.add(new Order(id,name,dateOrder,price,rating));
        }
        return list;
    }
    //getByName
    public List<Order> getByName(String nameItem) {
        String sql = "nameItem like ?";
        String[] args = { "%" + nameItem + "%" };
        SQLiteDatabase sqLiteDatabase = getReadableDatabase();
        Cursor cursor = sqLiteDatabase.query("OrderDB", null, sql, args, null, null, null);
        List<Order> orders = new ArrayList<>();
        while(cursor.moveToNext()) {
            Integer id = cursor.getInt(0);
            String name=cursor.getString(1);
            String dateOrder =cursor.getString(2);
            float price=cursor.getFloat(3);
            String rating = cursor.getString(4);
            Order order = new Order(id,name,dateOrder,price,rating);
            orders.add(order);
        }
        return orders;
    }

    //getBy id
    public Order getOrderById(int id){
        String whereClause="id =?";
        String[] whereArgs={String.valueOf(id)};
        SQLiteDatabase st=getReadableDatabase();
        Cursor cursor=st.query("OrderDB",null,whereClause,
                whereArgs,null,null,null);
        if(cursor.moveToNext()){

            String name=cursor.getString(1);
            String dateOrder =cursor.getString(2);
            float price=cursor.getFloat(3);
            String rating = cursor.getString(4);
            return new Order(id,name,dateOrder,price,rating);
        }
        return null;
    }
    //update
    public int update(Order order){
        ContentValues v=new ContentValues();
        v.put("nameItem",order.getNameItem());
        v.put("dateOrder",order.getDateOrder());
        v.put("price",order.getPrice());
        v.put("rating",order.getRating());

        String whereClause="id=?";
        String[] whereArgs={String.valueOf(order.getId())};
        SQLiteDatabase st=getWritableDatabase();
        getAll();
        return st.update("OrderDB",v,whereClause,whereArgs);
    }
    //delete
    public int delete(int id){
        String whereClause="id=?";
        String[] whereArgs={String.valueOf(id)};
        SQLiteDatabase st=getWritableDatabase();
        getAll();
        return st.delete("OrderDB",whereClause,whereArgs);
    }

}
