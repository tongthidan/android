package com.example.baiorder;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class OrderViewHolder extends RecyclerView.ViewHolder {
    TextView textViewId, textViewName, textViewPrice, textViewRating, textViewDateOrder;
    Button btnUpdate, btnDelete;
    public OrderViewHolder(@NonNull View v) {
        super(v);
        textViewId = v.findViewById(R.id.order_id);
        textViewName = v.findViewById(R.id.order_name);
        textViewDateOrder = v.findViewById(R.id.order_date);
        textViewPrice = v.findViewById(R.id.order_price);
        textViewRating = v.findViewById(R.id.order_rating);
        btnUpdate = v.findViewById(R.id.order_update);
//        btnDelete = v.findViewById(R.id.order_delete);
    }
}
