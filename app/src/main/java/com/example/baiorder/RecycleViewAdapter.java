package com.example.baiorder;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class RecycleViewAdapter extends RecyclerView.Adapter<OrderViewHolder> {
    private List<Order> list;
    SQLiteOrderHelper sqLite;
    public void RecyclerViewAdapter() {

        list=new ArrayList<>();
    }
    public void setsOrder(List<Order> list){

        this.list=list;
    }
    TextView textViewId, textViewName, textViewPrice, textViewRating, textViewDateOrder;
    Button btnUpdate;

    @NonNull
    @Override
    public OrderViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.order_card,parent,false);
        return  new OrderViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull OrderViewHolder holder, int position) {
        Order order = list.get(position);
        holder.textViewId.setText(order.getId()+" ");
        holder.textViewName.setText(order.getNameItem());
        holder.textViewDateOrder.setText(order.getDateOrder());
        holder.textViewPrice.setText(order.getPrice()+"");
        holder.textViewRating.setText(order.getRating());
        holder.btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Context context = v.getContext();
                Intent intent = new Intent(context, UpdateActivity.class);
                intent.putExtra("order",order);
                context.startActivity(intent);
            }
        });
//        holder.btnDelete.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                try{
//                    sqLite.delete(order.getId());
//                }catch(NumberFormatException e){
//                    System.out.println(e);
//                }
//            }
//
//        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
