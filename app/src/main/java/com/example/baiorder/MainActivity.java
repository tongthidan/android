package com.example.baiorder;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.MenuItemCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.SearchView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

public class MainActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    FloatingActionButton fab;
    RecycleViewAdapter adapter;
    SQLiteOrderHelper sqLite;
    private SearchView searchView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);





        recyclerView = findViewById(R.id.rev);
        fab = findViewById(R.id.fab);
        searchView = findViewById(R.id.searchView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new RecycleViewAdapter();
        sqLite=new SQLiteOrderHelper(this);
        adapter.setsOrder(sqLite.getAll());
        System.out.println("getAll");
        recyclerView.setAdapter(adapter);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, AddActivity.class);
                startActivity(intent);
            }
        });
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                List<Order> list=sqLite.getByName(newText);
                adapter.setsOrder(list);
                recyclerView.setAdapter(adapter);
                return true;
            }
        });

    }
    @Override
    protected void onRestart() {
        List<Order> list=sqLite.getAll();
        adapter.setsOrder(list);
        recyclerView.setAdapter(adapter);
        super.onRestart();
    }
}